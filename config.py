

## developer define variables

# 'argument passed by platform developer, the working dir on EC2 of the platform, the intermediate files and output file will be generated in workingDir
# e.g. /genomic-data2/00_ampliconSeq_cc
     #  workingDir =


# S3 path for user to download the allzip.zip file, developer please unzip it to access to the sub-directories for visulization
# e.g. s3://bhb-all/bhb-data/RawData/Pisum_sativum/Bionano_MGI/
     #  S3_out =


# S3 path to archive the whole project,
# e.g. s3://bhb-all/bhb-data/RawData/Pisum_sativum/Bionano_MGI/
     #  S3_archive =


# docker image name for crisper2 batch and single and, muscle
  ## change dokcer name '-i pinellolab/crispresso2' accordingly in function call_CRISPResso2_batch