import os
import pandas as pd
import subprocess
import logging
from os.path import join
from os.path import split
import matplotlib.pyplot as plt
import numpy as np





logger = logging.getLogger(__name__)  # __name__ variable will be interpert as __main__ if we run this script as main program, by doing this the info in the log will be distinguished, where is the error from
logger.setLevel(logging.INFO)

formatter = logging.Formatter('%(asctime)s:%(name)s:%(message)s')

file_handler = logging.FileHandler('BH_GEvulation.log')
file_handler.setLevel(logging.INFO)
file_handler.setFormatter(formatter)

stream_handler = logging.StreamHandler()  # have this line will report error to console
stream_handler.setFormatter(formatter)

logger.addHandler(file_handler)
logger.addHandler(stream_handler)




def fastq_suffix(): # tested
    '''
    the vendor provide fastq file in the format of eg. "9_S9_L001_R1_001.fastq.gz" but they do sometimes format with
    '9_S9_R1_001.fastq.gz', here is to determine the proper suffix
    :return:
    '''
    # f.rsplit('_', 3)[1:] ==> ['L001', 'R1', '001.fastq.gz']
    try:
        fastqname = [f for f in os.listdir() if f.endswith("fastq.gz")]

        if fastqname[0].split('_R1')[0].endswith('_L001'):
            fq_suffix = '_L001_R1_001.fastq.gz'
        else:
            fq_suffix ='_R1_001.fastq.gz'
    except Exception as e:
        logger.info('executing on function: fastq_suffix  ...')
        logger.exception(e)
    else:
        return fq_suffix



def get_unique_sitename(): # tested
    try:
        siteName_list_1 = submission_sheet['Site_Name'].tolist()
        siteName_list = []
        for site in siteName_list_1:
            if site not in siteName_list:
                siteName_list.append(site)
    except Exception as e:
        logger.info('executing on function get_unique_sitename  ...')
        logger.exception(e)
    else:
        return siteName_list





def df_reformat (df): # tested


    """
    reformat the dataframe into a dictionry of {key 1 (indexID): value 1 ("Site_Name", "Amplicon_Seq", "Target_gRNA", "Sample_ID", "Sample_Name")}
    the value is a list of such items

    """
    try:
        index_list = df['Index_ID'].tolist()

        # get the info list, each row of the dataframe, list of each_row_lists
        info_list = []
        for index, rows in df.iterrows():
            my_list = [rows.Site_Name, rows.Amplicon_Seq, rows.Target_gRNA, rows.Sample_ID, rows.Sample_Name]
            info_list.append(my_list)

        # build dictionary:  { 'A001': [1,1, 'ACTTTTTT...','gRNA..AAATC'],
        #                      'A002': [3,2, 'AGCGTTTT...','gRNA..AAAAT']}
        info_dic = dict(zip(index_list, info_list))
    except Exception as e:
        logger.info('executing on function df_reformat ...')
        looger.exception(e)
    else:
        return info_dic



def call_CRISPResso2_batch(info_dic, siteName_list, wc, ws, fastqfile_suffix):

    try: ###  NOT tested

        dic_list = []
        for i in range (0, len(siteName_list)):

            sub_info_dic = {key:value for key, value in info_dic.items() if value[0] == siteName_list[i]}

            dic_list.append(sub_info_dic)


        for n in range(0,len(dic_list)):

            #batch_prefix_0 = list(dic_list[n].values())
            batch_prefix_0 = [*dic_list[n].values()]

            batch_prefix = batch_prefix_0[0][0]

            batch_file = '{}_batch.batch'.format(batch_prefix)
            f = open(batch_file,'w')
            f.write(('n\tr1\n'))

            #each_dic_sample_list = list(dic_list[n].keys())  # e.g. get A003, A004, A005 for site gala
            each_dic_sample_list = [*dic_list[n].keys()]  # e.g. get A003, A004, A005 for site gala

            for m in range(0, len(each_dic_sample_list)):

                fname_1 = dic_list[n].get(each_dic_sample_list[m])[4] # first peroid in fname

                fname_2 = dic_list[n].get(each_dic_sample_list[m])[3] # second peroid in fname

                seq_fname = str(fname_1) + '_S' + str(fname_2) + fastqfile_suffix  # 1_S1_R1_001.fastq.gz p15
                #print(seq_fname)

                amp_Seq = dic_list[n].get(each_dic_sample_list[m])[1]
                gRNA_Seq = dic_list[n].get(each_dic_sample_list[m])[2]
                #print(each_dic_sample_list[m]+seq_fname)
                # write sample name and seq file into batch.batch file
                f.write(each_dic_sample_list[m] + '\t' + seq_fname + '\n')
            f.close()

            subprocess.call(["docker", 'run', "-v", os.getcwd()+"/:/DATA/:z", '-w', '/DATA' ' -i', 'pinellolab/crispresso2',"CRISPRessoBatch", "--batch_settings",batch_file,
                             "--amplicon_seq", amp_Seq, "-g", gRNA_Seq,"-n", 'nhej', '--quantification_window_center', wc, "--quantification_window_size", ws, '-p', '4'])
    except Exception as e:
        logger.info('executing on function call_CRISPResso2_batch ...')
        looger.exception(e)




def call_CRISPResso2_single(df,sample_Index,wc,ws, fastqfile_suffix):
# user should't specify the out dir, running on our workingDir, output to the workingDir, plafform admin should specify the workingDir
# http://crispresso2.pinellolab.org
# https://github.com/pinellolab/CRISPResso2

   try:
       for i in range (0, len(sample_Index)):
           fn_1 = df.get(sample_Index[i])[4]
           fn_2 = df.get(sample_Index[i])[3]
           seq_fn = str(fn_1) + '_S' + str(fn_2) + fastqfile_suffix

           amp_seq = df.get(sample_Index[i])[1]
           gRNA_seq = df.get(sample_Index[i])[2]

           subprocess.call(["docker", 'run', "-v", os.getcwd()+"/:/DATA/:z", '-w', '/DATA' ' -i', 'pinellolab/crispresso2', 'CRISPResso',
                '--fastq_r1', seq_fn, '--amplicon_seq', amp_seq, '--guide_seq', gRNA_seq, '-n', 'nhej',
                "--quantification_window_center", wc, '--quantification_window_size', ws, '--trim_sequences', 'True', '--file_prefix',sample_Index[i]])

   except Exception as e:
        logger.info('executing on function call_CRISPResso2_single ...')
        looger.exception(e)


def call_deletion_alleles(quant_center, quant_size,info_dic, oneOri_allele_file, suf_name): # tested

    # test with P14⁩ ▸ ⁨p14_crpes2⁩ ▸ ⁨CRISPRessoBatch_on_nhej-L2-8  A016 'Alleles_frequency_table.txt'

    try:
        ## FOR Version 2: docker cmd run under 'nhej' mode
        site_name = info_dic.get(suf_name)[0]
        amplicon_seq = info_dic.get(suf_name)[1]
        gRNA_seq = info_dic.get(suf_name)[2]  # test with "gtaccgatggtgaacaagtcctca"
        # p.center is the position on the amplicon seq, but should be coordinated with the user passed quant_center, and quant_size, therefore made such transform
        p_center = amplicon_seq.find(gRNA_seq) + len(gRNA_seq) -1 + quant_center # or the default set to the middle based on these length
        quant_start = p_center - quant_size + 1
        quant_end = p_center + quant_size # these 3 lines are tested to match the starting pos and end pos of gRNA on amplicon


        ## 2. subtract the data frame with reads has at least two base in "-", whatever the position and continuous or not to greatly downsize df

        alleles_frequency_df = pd.read_csv(oneOri_allele_file, sep='\t')  #?? the path to this file
        deletion_df_1 = alleles_frequency_df.loc[alleles_frequency_df['Read_Status'] == 'MODIFIED']
        deletion_df = deletion_df_1.loc[deletion_df_1['#Reads'] > 0]  #???? perserve or not

        ## 3. retain the aligned reads who has at least two'-' in the quantification window

        deletion_dict = dict(zip(deletion_df.iloc[:,0], deletion_df.iloc[:,7]))

        ###deletion_dict_2 = { (key, value) for (key, value) in deletion_dict.items() if key[quant_start:quant_end].count('--')>= 1}

        deletion_dict_3 = {(key, value) for (key, value) in deletion_dict.items() if key[quant_start:quant_end+1].count('-') >= 1}
        # ??? need to check deletion_dict_3, length, if it's already 0 then stop and report 0

        deletion_dict_3_df = pd.DataFrame.from_dict(deletion_dict_3)

        total_editreads = deletion_dict_3_df.iloc[:,1].sum()
        # test with for k,v in deletion_dict_2: print(k), check the output, every reads deletion was in the defined range.


        ## 4. write into fasta file, with header ">", record as number(1,2,3,4) : number of this reads detected in the alignment

        n=0 # counter for number of >
        file = open('./rare_allele/rare_allele_'+ suf_name +'.fa', 'w')
        file.write('>' + site_name + '\n' + amplicon_seq +'\n')
        for k, v in deletion_dict_3:
            n+=1

            file.write('>'+ str(n) + ':' + str(v) + '\n' + k.replace('-', '') + '\n')
        file.close()


        #
        #
        # plot figure for num of reads against deletions(bp)
        deletion_dict_4 = {(key[quant_start:quant_end+1], value) for (key, value) in deletion_dict.items() if key[quant_start:quant_end+1].count('-') >= 1}
        #{('T-GC', 1), ('TTC-', 1), ('GA--', 1), ('T-GA', 3), ('TT-A', 1), ('TTT-', 1), ('T-GA', 5), ('T-GA', 14),
         #('T-GA', 4), ('TTG-', 1), ('A-GA', 1), ('T-GA', 6), ('G---', 1), ('-TTT', 1), ('T-GA', 1), ('--GA', 1)}

        deletion_dict_5 = {(key[quant_start:quant_end+1].count('-'), value) for (key, value) in deletion_dict.items() if key[quant_start:quant_end+1].count('-') >= 1}
        # {(1, 3), (3, 1), (1, 14), (1, 4), (2, 1), (1, 5), (1, 6), (1, 1)}
        deletion_dict_5 = list(deletion_dict_5)

        sum = {}
        for (f_key,f_value) in deletion_dict_5:
            if f_key in sum:
                sum[f_key] = sum[f_key] + f_value
            else:
                sum[f_key] = f_value

        #sum = {(sum[f_key] : sum.get(f_key) + f_value if f_key in dele_num_list else sum[f_key] = f_value}
        #{(sum[f_key] if f_key in dele_num_list else sum[f_key]):(sum.get(f_key)+f_value if f_key in dele_num_list else f_value) for f_key, f_value in deletion_dict_5}
        sum_percentage = {key: round(float(value/total_editreads)*100,3) for (key, value) in sum.items()}


        #dele_var_plot = plt.bar(list(sum_percentage.keys()), sum_percentage.values(), color='g')
        plt.bar(list(sum_percentage.keys()),sum_percentage.values(),color='g')
        #plt.xticks((np.arrange(min(sum_percentage.keys()), max(sum_percentage.keys()), 1.0)))

        x = list(sum_percentage.keys())
        y = list(sum_percentage.values())
        plt.bar(x,y)

        plt.xticks(np.arange(min(x), max(x) + 1, 1.0))
        plt.xlabel(suf_name + ': variations of deletion in QW (bp)')
        plt.ylabel('percentage of deletion events in totoal edited reads (%)')
        plt.savefig('./deletion_variation_plot/dele_var_'+ suf_name +'.png')

        #deletion_variation_plot
        #print (total_editreads)
    except Exception as e:
        logger.info('executing on function call_deletion_alleles ...')
        looger.exception(e)
    else:
        return total_editreads




def rare_alle_calculation_batch(QuantCenter, QuantSize, info_dic, prefix):

    try:

        with open('dele-eff_select-resultbatch' + prefix + '.txt', mode='a') as bo_file:

            bo_file.write('Index_ID' + '\t' + 'Edited_reads' + '\t' + 'Aligned_reads' + '\t' + 'Editing_efficiency%' + '\n')

        for root, dirs, files in os.walk('.'):

            for file in files:
                file = os.path.join(root, file)
                if file.endswith("Alleles_frequency_table.zip"):

                    #subprocess.Popen(['unzip', '*.zip'])  # Alleles_frequency_table.txt
                    p2f = os.path.abspath(file)

                    path, fn = os.path.split(p2f)
                    os.system('unzip ' + p2f + " -d" + " " + path)

                    dir_name = path.split('/')[-2]
                    dir_name_p1, suf_name = dir_name.split('CRISPResso_on_')  # suf_name = A001
                    #print(suf_name)
                    quant_center, quant_size, info_dic, oneOri_allele_file, suf_name = QuantCenter, QuantSize, info_dic, path+"/Alleles_frequency_table.txt", suf_name
                    total_editreads = call_deletion_alleles(quant_center, quant_size, info_dic, oneOri_allele_file, suf_name)  # write the rare_allele file and the num of edit reads

                    ori_summary_df = pd.read_csv('./CRISPRessoBatch_on_nhej/CRISPResso_on_' + suf_name + '/CRISPResso_on_nhej/CRISPResso_quantification_of_editing_frequency.txt', sep='\t')
                    total_aligned_reads = ori_summary_df.iloc[0, ori_summary_df.columns.get_loc('Reads_aligned')]

                    edit_rate = round((total_editreads / total_aligned_reads * 100),3)

                    with open('dele-eff_select-resultbatch' + prefix + '.txt', mode='a') as bo_file:
                        bo_file.write(suf_name + '\t' + str(total_editreads) + '\t' + str(total_aligned_reads) + '\t' + str(
                            edit_rate) + '\n')

    except Exception as e:
        logger.info('executing on funtion rare_alle_calculation_batch ...')
        logger.exception(e)








def rare_alle_calculation_select(QuantCenter, QuantSize, info_dic,prefix):

    try:

        with open('dele-eff_select-result' + prefix + '.txt', mode='a') as so_file:
            so_file.write('Index_ID' + '\t' + 'Edited_reads' + '\t' + 'Aligned_reads' + '\t' + 'Editing_efficiency%' + '\n')

        for root, dirs, files in os.walk('.'):
            for file in files:
                file = os.path.join(root, file)
                if file.endswith("Alleles_frequency_table.zip"):
                    #os.system('unzip '+file)
                    #subprocess.Popen(['unzip', '*.zip'])  # Alleles_frequency_table.txt
                    p2f = os.path.abspath(file)

                    path, fn = os.path.split(p2f)

                    os.system('unzip ' + p2f +" -d"+" "+path)
                    #dir_name = os.path.basename(path3)
                    dir_name = path.split('/')[-2]

                    quant_center, quant_size, info_dic, oneOri_allele_file, suf_name = QuantCenter, QuantSize, info_dic, path+"/Alleles_frequency_table.txt", dir_name

                    #call_deletion_alleles(quant_center, quant_size, info_dic, oneOri_allele_file, suf_name)

                    total_editreads = call_deletion_alleles(quant_center, quant_size, info_dic, oneOri_allele_file, suf_name)
                    ori_summary_df = pd.read_csv(dir_name + '/CRISPResso_on_nhej/CRISPResso_quantification_of_editing_frequency.txt', sep='\t')
                    total_aligned_reads = ori_summary_df.iloc[0, ori_summary_df.columns.get_loc('Reads_aligned')]

                    edit_rate = round((total_editreads / total_aligned_reads * 100),3)

                    with open('dele-eff_select-result' + prefix + '.txt', mode='a') as so_file:
                        so_file.write(dir_name + '\t' + str(total_editreads) + '\t' + str(total_aligned_reads) + '\t' + str(edit_rate) + '\n')

    except Exception as e:
        logger.info('executing on funtion rare_alle_calculation_select ...')
        logger.exception(e)


def rare_allele_alignment():

    try:

        for file in os.listdir('rare_allele'):
            if file.endswith('.fa'):
                p2file = os.path.abspath(file)

                print('muscle ', '-in ', f2file, ' -clw', ' -out', f2file+".aln", ' -clwstrict')

                print('muscle ', '-in ', f2file, '-html', ' -out', f2file+".html", ' -clwstrict')

    except Exception as e:
        logger.info('executing on function: rare_allele_alignment  ...')
        logger.exception(e)





def collect_qw_plot(path2d):

    try:

        for root, directories, qw_fns in os.walk(path2d): # './CRISPRessoBatch_on_nhej/'
            for qw_fn in qw_fns:
                if qw_fn.startswith('2b'):
                    path_qw_fn = (os.path.join(root, qw_fn))
                    # print(path_qw_fn)  # ./dir_1/test1.file

                    qw_dirname = path_qw_fn.split('/')[-2]
                    qw_prefix = qw_dirname.split('_')[-1]

                    p2_qw_fn_new = os.path.join(root, qw_prefix + "_" + qw_fn)
                    os.rename(path_qw_fn, p2_qw_fn_new)

                    # print(p2_qw_fn_new)
                    os.system('cp ' + p2_qw_fn_new + " ./quantification-window_plot")

    except Exception as e:
        logger.info('executing on function: collect_qw_plot  ...')
        logger.exception(e)




if __name__ == '__main__':

    print('running GeneEditEvaluation_modules.py!')

#



