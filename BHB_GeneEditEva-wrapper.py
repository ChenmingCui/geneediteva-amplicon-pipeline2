# check if input file exists
# check if the input file has proper necessy headers
# check if the software installed properly

# pass arguments to each of the steps
# in paraell computing
# collect all the files from like S3 to a workingDir in EC2 ....?    # check N, write into log, butkit -- markdown
import sys
import os
import pandas as pd
import subprocess
import argparse
import logging
import config
import GeneEditEvaluation_modules





def get_args():
    ''' Get arguments '''
    parser = argparse.ArgumentParser(description='Benson Hill Biosystems -- Gene editing (deletion events) evaluation using amplicon sequencing, Sep. 2019, cc\n')
    parser.add_argument('--version', action='version', version='GeneEditEvaluation2.0.0')

    ###$$$$ . user pass variable to the analysis
    parser.add_argument('path2submissionSheet', help='user browse the S3 directory, choose proper directory where user put submission sheet')
    parser.add_argument('path2fastq',help='user browse the S3 directory, choose proper directory where user put the fastq.zip file')
    parser.add_argument('run_mode', choices=['Batch2go', 'Selected'], help='choose ONE from Batch2go and Selected')
    parser.add_argument('QuantCenter', type=int, help='suggested value -12, the quantification center of the quntificaiton window, end of gRNA in 5 to 3 direction is 0, left upper direction is negative value')
    parser.add_argument('QuantSize', type=int, help='suggested value 12, the quantification size on one side of the quantification center')
    parser.add_argument('prefix', type=str, help='user should prefix your analysis, each analysis should has unique prefix, e.g. P14, P14-test1, P14-08282019 (no space in prefix name)')

    parser.add_argument('--selected_sample', '--list', nargs='+', help='user input index ids from spread sheet')  # collected samples into a list
    return parser.parse_args()




def main():
    ## variables, path2submissionSheet, path2fastq, work_Dir
    """

    :param workingDir: platform developer define, the working dir on EC2 (????) of the platform, the intermediate files and output file will be generated in workingDir

    :param path2submissionSheet: the path to the dir on S3 (???) that has "_submissionsheet.xlsx" from gene editing group
                                 e.g.  "//bhb-all/bhb-data/RawData/T7_sample_submissionsheet.xlsx"

                                 required_headers = ["Index_ID", "Site_Name", "Amplicon_Seq", "Target_gRNA", "Sample_ID", "Sample_Name"] please follow the column order in spreadsheet, we'll have a template sheet

                                 "Index_ID": unique sample ids across the entire dataset
                                 "Site_Name": unique Name for samples with identical "Aamplicon Seq" and "Target_gRNA"
                                 "Sample_Name": info returned from the vendor "SampleSheet-2"
                                 "Sample_ID": info returned from the vendor "SampleSheet-2"

    :param path2fastq: the path to the dir that has the zipped fastq file, '_fastq.zip'. eg. "190822_M00645_0006_BensonHill_P15-CKNML_fastq.zip"
                       e.g. "//bhb-all/bhb-data/RawData/190809_M01238_0255_Benson_Hill_T7-CL26R_fastq.zip"

    :param run_mode: Batch2Go (entire datasets from the submission sheet) or Selected (single or multiples samples, type in Index_IDs(A001, A009)), Batch2Go by default

    :param quantCenter: integers, recommended -12 (middle position of gRNA). End position of the 5'-->3' gRNA on the amplicon sequence will be counted as +1

    :param quantSize: integers, recommended 12, length of bases covering one side of QuanCenter.

    :param prefix: prefix of your analysis, e.g. P14, P14-test1, P14-08282919 (no space in prefix name); each of run submission should have unique prefix




    :return: _submission_sheet" with appended results, alignment file

    help: help(Main)
    """

    ## create log file
    ### set up log files
    logger = logging.getLogger(__name__)                    # __name__ variable will be interpert as __main__ if we run this script as main program, by doing this the info in the log will be distinguished, where is the error from
    logger.setLevel(logging.INFO)

    formatter = logging.Formatter('%(asctime)s:%(name)s:%(message)s')

    file_handler = logging.FileHandler('BH_GEvalation.log')
    file_handler.setLevel(logging.INFO)
    file_handler.setFormatter(formatter)

    stream_handler = logging.StreamHandler()                 # have this line will report error to console
    stream_handler.setFormatter(formatter)

    logger.addHandler(file_handler)
    logger.addHandler(stream_handler)





    ### get arguments variables
    args = get_args()
    #print(args)
    # path2submissionSheet, path2fastq, run_mode = args.path2submissionSheet, args.path2fastq, args.run_mode
    # QuantCenter, QuantSize, prefix, selected_sample = args.QuantCenter, args.QuantSize, args.prefix, args.selected_sample


    #######################################        item 0: define projectDir under workingDir on EC2  ##################
    #######################################                                                     ########################
    if config.workingDir[-1] != '/': config.workingDir += '/'
    projectDir = config.workingDir+args.prefix


    #######################################        item 1: Prep. files             #####################################
    #######################################                                        #####################################

    ## 1.0 make project dir under working Dir
    try:

        os.mkdir(projectDir)

        os.chdir(projectDir)

        os.mkdir('rare_allele')  # collect alignment outcome
        os.mkdir('deletion_variation_plot')
        os.mkdir('quantification-window_plot')
    except Exception as e:
        logger.info("Genome Editing Evaluation from Amplicon Sequencing Data v.2.0 -- Benson·Hill \n\n")
        logger.info("step 1.0 Creating project directory and the sub-directories under working Directory...")
        logger.exception(e)
        raise # it will stop the execution



    ## 1.1 copy files from the archived dir (S3) to the project directory
    try:## need exception for specific???????

        os.system('aws s3 cp s3:'+args.path2submissionSheet+' '+projectDir) #need test # //bhb-all/bhb-data/RawData/Pisum_sativum/
        os.system('aws s3 cp s3:'+args.path2fastq+' '+projectDir) # need test
        ## eg.aws s3 cp s3://bhb-all/bhb-data/RawData/Pisum_sativum/Bionano_MGI/Pea_Bionano_Project_Report.pptx /genomic-data2/00_ampliconSeq_cc
        ## unzip the fastq.zip file

        os.system('unzip *.zip')

    except Exception as e:
        logger.info("step 1.1 copy submission sheet from S3...")
        logger.exception(e)
    except Exception as e:
        logger.info('step 1.1 copy zipped fastq file from S3...')
        logger.exception(e)
    except Exception as e:
        logger.info('step 1.1 zipping the fastq file...')
        raise


    ## 1.2 obtain the proper fastq file suffix

    fq_suffix = GeneEditEvaluation_modules.fastq_suffix()

    logger.info('PASS Item1. files are prepared, fastq file name format was determined.\n\n\n')





    #######################################        item 2: Pre-check                  ##################################
    #######################################                                           ##################################

    ## 2.0 read the submission sheet
    #submission_sheet = pd.read_excel(path2submissionSheet) # you don't know the submission sheet name
    try:
        submissionSheet_list = [fname for fname in os.listdir('.') if fname.endswith('_submissionsheet.xlsx')]
        submission_sheet = pd.read_excel(submissionSheet_list[0])
    except Exception as e:
        logger.info('step 2.0 read submission sheet in the project directory...')
        logger.exception(e)
        raise


    ## 2.1 check if the required columns are satisfied
    in_header_list = (submission_sheet.columns.values)[0:6].tolist()

    required_headers = ["Index_ID","Site_Name","Amplicon_Seq", "Target_gRNA", "Sample_ID", "Sample_Name"]

    check1 = in_header_list == required_headers
    if check1 == False:

        logger.error('ERROR Item2.1 Your submissionSheet should have at least those columns in order: "Index_ID", "Site_Name", "Amplicon_Seq", "Target_gRNA", "Sample_ID", "Sample_Name", make sure your column names and order matched \n')

        raise Exception('Error item 2.1: Your submissionSheet should have at least those columns in order: "Index_ID", "Site_Name", "Amplicon_Seq", "Target_gRNA", "Sample_ID", "Sample_Name", make sure your column names and order matched')

    else:

        logger.info('PASS Item2.1 You passed submissionsheet header check!\n')



    ## 2.2 check if index_ids are unique, otherwise program will not execute
    index_ids = submission_sheet['Index_ID'].tolist()

    if len(index_ids) > len(set(index_ids)):

        logger.error('ERROR Item2.2 Your ids in Index_ID column of _submissionsheet.xlsx are not unique, making sure they are unique ID for each sample\n')

        raise Exception ('Your ids in Index_ID column of _submissionsheet.xlsx are not unique, making sure they are unique ID for each sample')

    else:

        logger.info('PASS Item2.2 You passed ID (in Index_ID column) uniqueness check!\n\n\n')



    ## 2.4 get the site names, index ids into list and reformat the dataframe to key, values(list of itmes) using function extract_info
    #siteName_list = get_unique_sitename()
    #submission_sheet_sample_index_list = submission_sheet['Index_ID'].tolist()
    #info_dic = df_reformat(submission_sheet)





    #######################################        item 3: call CRISPRESSO-2          ##################################
    #######################################                                           ##################################

    ## 3.0 run CRISPREsso2 with the requested mode, extract rara alleles to rare_allele dir and do the calculation

    if args.run_mode == 'Selected':

        check2 = len(args.selected_sample) # selected_sample is a list of index ids
        submission_sheet_sample_index_list = submission_sheet['Index_ID'].tolist()
        check3 = set(args.selected_sample).intersection(set(submission_sheet_sample_index_list)) == set(args.selected_sample)

        if check2 == 0:

            logger.error ('ERROR Item3.0-selected, You have to input at least one Index ID if you are trying to run with selected mode\n')

            raise Exception ('You have to input at least one Index ID')

        elif check3 == False:


            logger.error ('ERROR Item3.0-selected, your input index ID is not valid,make sure your index IDs match IDs in the Index_ID of your submission sheet\n')

            raise Exception ('Your input index ids is not valid')

        else:
            logger.info ('Your input index IDs are good to go!')
            info_dic = GeneEditEvaluation_modules.df_reformat(submission_sheet)
            df, sample_Index, wc, ws,fastqfile_suffix = info_dic, args.selected_sample, args.QuantCenter, args.QuantSize, fq_suffix

            GeneEditEvaluation_modules.call_CRISPResso2_single(df, sample_Index, wc, ws, fastqfile_suffix) # tested works

            # call rare alleles and calculate the deletion events
            QuantCenter, QuantSize, info_dic,prefix = args.QuantCenter, args.QuantSize, info_dic, args.prefix
            GeneEditEvaluation_modules.rare_alle_calculation_select(QuantCenter, QuantSize, info_dic,prefix) # tested works


            logger.info ('PASS Item3.0-selected, deletion alleles were called and calculated\n')

    else:
        siteName_list = GeneEditEvaluation_modules.get_unique_sitename()
        info_dic = GeneEditEvaluation_modules.df_reformat(submission_sheet)
        info_dic, siteName_list, wc, ws, fastqfile_suffix = info_dic, siteName_list, args.QuantCenter, args.QuantSize, fq_suffix
        GeneEditEvaluation_modules.call_CRISPResso2_batch(info_dic, siteName_list, wc, ws, fastqfile_suffix) # tested works
        # call rare alleles and calculate the deletion events

        QuantCenter, QuantSize, info_dic, prefix = args.QuantCenter, args.QuantSize, info_dic, args.prefix
        GeneEditEvaluation_modules.rare_alle_calculation_batch(QuantCenter, QuantSize, info_dic, prefix)


        logger.info('PASS Item3.0-batch2go, deletion alledles were called and calculated\n')




    #######################################        item 4: call "muscle"              ##################################
    #######################################        rare allele alignment              ##################################
    try:

        GeneEditEvaluation_modules.rare_allele_alignment()

    except Exception as e:

        logger.exception(e)



    #######################################        item 5: prepare result             ##################################
    #######################################                                           ##################################

    ## 5.0 collect the quantification window plot
    try:
        GeneEditEvaluation_modules.collect_qw_plot('./CRISPRessoBatch_on_nhej/')

    except Exception as e:
        logger.info("ERROR step 5.0: error in collect qw plots from main program...")
        logger.exception(e)

    ## 5.1 merge the edit result with the user's origianl submission info sheet
    try:
        for file in os.listdir('.'):
            if file.startswith('dele-eff'):
                cc_result_df = pd.read_csv(file, sep='\t')
                final_df = pd.merge(submission_sheet, cc_result_df,
                                    on="Index_ID")  # tested, will generated the cc_result_df length df
                final_df.to_excel(args.prefix + "_spreadsheet_out.xlsx")
    except Exception as e:
        logger.info("ERROR step 5.1: error in generating final info sheet...")
        logger.exception(e)

    ## 5.1 zip file and dirs into _allzip.zip for user download

    os.system('zip -r '+args.prefix+'_allzip rare_allele deletion_variation_plot quantification-window_plot '+args.prefix+'_spreadsheet_out.xlsx') # ==> p12_allzip.zip




    #######################################        item 6: clean workingDir           ##################################
    #######################################                                           ##################################
    try:
        ## 6.1 remove fastq files

        os.system('rm *.fastq.gz')

        ## 6.2 archive _allzip.zip to AWS S3 for user download and visulization on EDIT
        os.system('aws s3 cp '+args.prefix+'_allzip.zip '+config.S3_out)
    except Exception as e:
        logger.info("ERROR step 6 : error in remove fastq file and zip allzip...")
        logger.exception(e)


    ## 6.3 archive the whole project to AWS S3
    os.system('cd ..')
    #os.system('mv ' + logfile.name + ' '+prefix) #move logfile into the the project directory

    os.system('zip '+args.prefix+'-analysis-out '+args.prefix) # zip the project in the working directory

    os.system('aws s3 mv '+args.prefix+'-analysis-out.zip '+config.S3_archive)
    # e.g. aws s3 mv test-r05-analysis-out.zip s3://bhb-all/bhb-data/RawData/Pisum_sativum/Bionano_MGI/

if __name__ == "__main__":

    #print('python3 BHB_GeneEditEva-wrapper.py -h')
    #print('Developer set up variable in the config.py file')
    main()


#### handle exception https://www.techbeamers.com/use-try-except-python/

## merge the alle file with spread sheet
### clean the workingDir




# get file's dirname

# alter the docker image ('pinellolab/crispresso2') in the batch (call_CRISPResso2_batch) and single (call_CRISPResso2_single) module accordingly
# muscle docker...
# Main variable, and if __name == main__


# /genomic-data2/00_ampliconSeq_cc/11_T08_01/CRISPRessoBatch_on_nhej/CRISPResso_on_A001
# /opt/conda/bin/CRISPRessoBatch --batch_settings batch03.batch --amplicon_seq tggatctcaaaatgtgcaggccacaaatattacttgtggtccaggtcatggtataaggtactctattttacaaatatacttgtttccattttctctatttcataaaaggtagtatgatataataattactttaaatcctttaattaatttattggcaaattttttctcttgtctttatggttaatgacttagcacaa
# -p 4 -g catggtataaggtactctatttta -n nhej --quantification_window_center -12 --quantification_window_size 12


#===>  /genomic-data2/00_ampliconSeq_cc/11_T08_01/CRISPRessoBatch_on_nhej/CRISPResso_on_A001   ## different bach will all go to CRISPRessoBatch_on_nhej, A001 was added to the
# CRISPResso_on_A001


#get file's path
#path = os.path.abspath('testfile')  ## '/Users/ccui/Documents/OneDrive-BensonHillBiosystems/pipeline_crisperAsse/testfile'
#pat = path.split('/')[-2] ## 'pipeline_crisperAsse'

## issues:
# 1. rare_allele_alignment --- dockerize ...then go for altering the code in the function
# 2. if args.run_mode = selected... how...without the botton.. done()
# 3. input window size to check the window size plot
# 4. call_deletion_alleles -- done! no need to run call it in main()

## final test
