# Gene editing evaluation using amplicon sequencing 2.0

####
Author: Chenming Cui @ Benson Hill Biosystems

##
Introduction:
###
- Benson Hill Biosystems -- Gene editing evaluation pipeline 2.0 is designed to leverage Next Generation Sequencing (NGS) --amplicon sequencing data
rapidly evaluate gene edit (NHEJ) efficiency in designed gene editing system
- Input 1). "xx_submissionSheet.xlsx", an excel spread sheet describes the particular gene editing systems. The required columns should be put in order of
required_headers = ["Index_ID", "Site_Name", "Amplicon_Seq", "Target_gRNA", "Sample_ID", "Sample_Name"], template spread sheet will be provided for users
2). "_fastq.zip". eg. "190822_M00645_0006_BensonHill_R05-CKNML_fastq.zip", a zipped fastq file that returned from sequencing vendor(MOgene), pipeline will unzip it and generate
fastq file for each sample
- Output 1). "spreadsheet_out.xlsx", "reads number has the deletion", "total aligned reads", "editing efficiency%" appends to each sample(rows in the spread sheet
2). '_aligned.zip', reads with deletions in the designed region of each sample(each row in the spread sheet) are performed alignment analysis, an aligned.fa file and aligned.html file
are generated for each sample. All the files are zipped into the '_aligned.zip' file for user's visualization and analysis

- ps. sample in-out files are archived in 'sampleData-in-out' of this bucket

##
Workflow:
###
- item 0: define projectDir under workingDir on EC2  
- item 1: Prep. files (copy the input files from S3 bucket to workingDir etc.)
- item 2: Pre-check  
- item 3: call CRISPResso2, in two running modes
- item 4: call "muscle" alignment
- item 5: prepare result
- item 6: clean workingDir (archive output files to S3 bucket etc.)
- ps. detailed steps are annotated in 'BHB_GeneEditEva-wrapper.py'

##
Dependencies and Requirements:
###
- python 3x
- python modules
  * BioPython
  * pandas
  * matplotlib
  * numpy
- software: CRISPResso2 [link](https://github.com/pinellolab/CRISPResso2), MUSCLE [link](http://www.drive5.com/muscle)
- ps. Dependencies and software will be built with docker, please check the provided dockerfile.
- ?? dockerfile: add these items to the original CRISPesso2 dockerfile: 1). USER root 2). python modules: pandas, datetime (pip3 or conda?) 3). install muscle:
  https://www.drive5.com/muscle/downloads3.8.31/muscle3.8.31_i86linux64.tar.gz
##
Usage:


```
Reminder: set up variables in config.py file

## developer define variables

working dir on EC2 of the platform, the intermediate files and output file will be generated in workingDir
e.g. /genomic-data2/00_ampliconSeq_cc
       workingDir =


S3 path for user to access the two output files,
e.g. s3://bhb-all/bhb-data/RawData/Pisum_sativum/Bionano_MGI/
       S3_out =


S3 path to archive the whole project,
e.g. s3://bhb-all/bhb-data/RawData/Pisum_sativum/Bionano_MGI/
       S3_archive =


# alter docker image name accordingly for crisper2 batch and single and, muscle




usage: BHB_GeneEditEva-wrapper.py [-h] [--version]
                                  [--selected_sample SELECTED_SAMPLE [SELECTED_SAMPLE ...]]
                                  path2submissionSheet path2fastq
                                  {Batch2go,Selected} QuantCenter QuantSize
                                  prefix
positional arguments:
  path2submissionSheet  user browse the S3 directory, choose proper directory
                        where user put submission sheet
  path2fastq            user browse the S3 directory, choose proper directory
                        where user put the fastq.zip file
  {Batch2go,Selected}   choose ONE from Batch2go and Selected
  QuantCenter           suggested value -12, the quantification center of the
                        quntificaiton window, end of gRNA in 5 to 3 direction
                        is 0, left upper direction is negative value
  QuantSize             suggested value 12, the quantification size on one
                        side of the quantification center
  prefix                user should prefix your analysis, each analysis should
                        has unique prefix, e.g. P14, P14-test1, P14-08282019
                        (no space in prefix name)

optional arguments:
  -h, --help            show this help message and exit
  --version             show program's version number and exit
  --selected_sample SELECTED_SAMPLE [SELECTED_SAMPLE ...], --list SELECTED_SAMPLE [SELECTED_SAMPLE ...]
                        user input index ids from spread sheet

```

###
[Contact me!](ccui@bensonhillbio.com)